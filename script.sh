#!/usr/bin/env bash
echo "Running mvn clean"
mvn clean install
retVal=$?
if [ $retVal -eq 0 ]; then
	echo "Install success"
	tar -zxvf server-app/target/server-app-1.0-SNAPSHOT.tar.gz -C server-app/target
	tar -zxvf client-app/target/client-app-1.0-SNAPSHOT.tar.gz -C client-app/target
	gnome-terminal -e "sh ./server-app/target/server-app-1.0-SNAPSHOT/bin/server-app console"
        sleep 1s
	gnome-terminal -e "sh ./client-app/target/client-app-1.0-SNAPSHOT/bin/client-app console"
fi
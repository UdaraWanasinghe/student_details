package com.udara;

import org.tanukisoftware.wrapper.WrapperManager;

/**
 * Hello world!
 */
public class ClientApp {
    public static void main(String[] args) {
        WrapperManager.start(new WrapperListenerService(), args);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Prompt prompt = new Prompt();
                prompt.process();
            }
        }).start();
    }
}

package com.udara;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

class ClientService {
    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 4099;
    private static final int REPLY_TIMEOUT = 2000;

    private Socket socket;
    private OutputStream mOutputStream;
    private InputStream mInputStream;

    private Logger logger = LogManager.getLogger(ClientService.class);

    private ClientListener mClientListener;

    boolean connect() {
        try {
            if (isConnected()) {
                logger.debug(
                        "ClientService is already connected to: " + SERVER_IP + ":" + SERVER_PORT
                                + "\nClientService connection should be stopped before attempting new connection.");
                return true;
            }

            // connect to the server
            socket = new Socket(SERVER_IP, SERVER_PORT);
            mInputStream = new BufferedInputStream(socket.getInputStream());
            mOutputStream = new BufferedOutputStream(socket.getOutputStream());

            return true;

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return false;
    }

    void sendData(String data) {
        if (isConnected()) {
            try {
                mOutputStream.write(data.getBytes());
                mOutputStream.flush();

                logger.debug("Waiting for reply");
                readReply();

            } catch (IOException e) {
                logger.error(e.getMessage());
            }

        } else {
            logger.debug("ClientService is not connected or already closed.");

        }
    }

    private void readReply() throws IOException {
        byte[] buffer = new byte[1024];

        StringBuilder stringBuilder = new StringBuilder();

        int read;

        long startTime = System.currentTimeMillis();

        while (true) {
            if (mInputStream.available() > 0) {
                read = mInputStream.read(buffer);

                stringBuilder.append(new String(buffer, 0, read));

                String str = stringBuilder.toString();

                String json = PatternMatcher.findJsonPattern(str);

                if (json != null) {
                    if (mClientListener != null) {
                        mClientListener.onJsonReceive(json);
                        closeConnection();
                        break;
                    }
                }

                long endTime = System.currentTimeMillis();

                if (endTime - startTime > REPLY_TIMEOUT) {
                    if (mClientListener != null) {
                        mClientListener.onConnectionFailed(new Exception("Timeout triggered"));
                    }
                    closeConnection();
                    break;
                }
            }
        }
    }

    /**
     * add listener for onJsonReceive and onConnectionFailed events
     *
     * @param clientListener {@link ClientListener}
     */
    void addListener(ClientListener clientListener) {
        this.mClientListener = clientListener;
    }

    /**
     * Close if there is an active connection
     */
    private void closeConnection() {
        if (isConnected()) {
            try {
                socket.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.debug("ClientService is not connected or already closed.");
        }
        socket = null;
    }

    /**
     * check whether there is an active connection
     *
     * @return true if there is an ongoing connection, else false
     */
    private boolean isConnected() {
        return socket != null && socket.isConnected() && !socket.isClosed();
    }

    interface ClientListener {
        void onJsonReceive(String data);

        void onConnectionFailed(Exception e);
    }
}

package com.udara;

import com.udara.DataModels.StudentData;
import com.udara.DataModels.StudentDataReply;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Prompt implements ClientService.ClientListener {
    private Logger logger = LogManager.getLogger(Prompt.class);

    public static void main(String[] args) {

    }

    /**
     * Get inputs from console
     */
    private StudentData getInputs() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("----------------------------------");
        System.out.println("--------Input student data--------");
        System.out.println("----------------------------------");

        System.out.println("Index no: ");
        String indexNo = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Name    : ");
        String name = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Address : ");
        String address = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Age     : ");
        String age = scanner.nextLine();
        System.out.println("----------------------------------");

        scanner.close();

        return new StudentData(indexNo, name, address, age);
    }

    /**
     * start caller process
     */
    void process() {
        ClientService clientService = new ClientService();
        clientService.addListener(this);
        boolean isConnected = clientService.connect();

        if (isConnected) {
            StudentData studentData = getInputs();

            String encodedData = Codec.encodeStudentData(studentData);

            clientService.sendData(encodedData);

        } else {
            logger.debug("Connection failed");
        }
    }

    @Override
    public void onJsonReceive(String data) {
        StudentDataReply receivedData = Codec.decodeReplyData(data);

        logger.debug("Reply: " + receivedData.message);
    }

    @Override
    public void onConnectionFailed(Exception e) {
        logger.error(e.getMessage());
    }
}

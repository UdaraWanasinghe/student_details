package com.udara;

import com.udara.DataModels.StudentData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

class DatabaseService {
    private static final String HOST = "localhost";
    private static final int PORT = 3306;
    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String DATABASE = "student_details";

    // tables
    private final String DETAILS_TABLE = "details";

    // columns
    private final String INDEX_COLUMN = "_index";
    private final String NAME_COLUMN = "_name";
    private final String ADDRESS_COLUMN = "_address";
    private final String AGE_COLUMN = "_age";

    private Connection mConnection;

    private static Logger logger = LogManager.getLogger(DatabaseService.class);

    private static DatabaseService databaseService;

    private DatabaseService() {

    }

    /**
     * get database service instance
     */
    static DatabaseService getInstance() {

        if (databaseService == null) {
            databaseService = new DatabaseService();

            try {
                databaseService.connect();

            } catch (SQLException e) {
                logger.error(e.getMessage());

            }
        }

        return databaseService;
    }

    static void start() {
        getInstance();
    }

    /**
     * Initialize database
     */
    private void connect() throws SQLException {
        logger.debug("Connecting to database server");

        mConnection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s?user=%s&password=%s",
                HOST, PORT, DATABASE, USER, PASSWORD));

        logger.debug(mConnection.isValid(2000) ? "Connection valid" : "Connection not valid");

        logger.debug("Database server connection success");

        createTablesIfNotExist();
    }

    /**
     * Creates details table in the database
     */
    private void createTablesIfNotExist() {
        try {
            Statement statement = mConnection.createStatement();

            String createTablesQuery = String.format("CREATE TABLE %s(" +
                            "%s varchar(7) NOT NULL," +
                            "%s varchar(32) NOT NULL," +
                            "%s varchar(64) NOT NULL," +
                            "%s int," +
                            "PRIMARY KEY(%s));",
                    DETAILS_TABLE,
                    INDEX_COLUMN,
                    NAME_COLUMN,
                    ADDRESS_COLUMN,
                    AGE_COLUMN,
                    INDEX_COLUMN);

            statement.executeUpdate(createTablesQuery);

            logger.debug("Created details table");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    void close() {
        try {
            mConnection.close();

        } catch (SQLException e) {
            logger.error(e);
        }
    }

    boolean create(StudentData studentData) {
        logger.debug("Insert called");

        try {
            Statement statement = mConnection.createStatement();

            String query = String.format(
                    "INSERT INTO %s(%s, %s, %s, %s) VALUES ('%s', '%s', '%s', '%s');",
                    DETAILS_TABLE,
                    INDEX_COLUMN,
                    NAME_COLUMN,
                    ADDRESS_COLUMN,
                    AGE_COLUMN,
                    studentData.indexNo,
                    studentData.name,
                    studentData.address,
                    studentData.age
            );

            logger.debug("Executing query");

            statement.executeUpdate(query);

            logger.debug("Query completed");

            return true;

        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    void read() {
    }

    void update(int id, StudentData studentData) {
    }

    void delete(int id) {
    }

}

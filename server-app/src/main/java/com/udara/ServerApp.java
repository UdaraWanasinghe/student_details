package com.udara;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tanukisoftware.wrapper.WrapperManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Database server helper app
 */
public class ServerApp {
    private static final int SERVER_PORT = 4099;

    private static Logger logger = LogManager.getLogger(ServerApp.class);

    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    private static ServerApp serverApp;

    private ServerSocket mServerSocket;

    public static void main(String[] args) {
        WrapperManager.start(new WrapperListenerService(), args);
        DatabaseService.start();
        ServerApp.start();
    }

    private ServerApp() {

    }

    private static void start() {

        if (serverApp == null) {
            serverApp = new ServerApp();

            serverApp.connect();

        }
    }

    private void connect() {
        try {
            logger.debug("Starting server");

            mServerSocket = new ServerSocket(SERVER_PORT);

            logger.debug("Waiting for connection");

            while (isConnected()) {
                Socket socket = mServerSocket.accept();

                logger.debug("Connection received: " + socket.getInetAddress().toString());

                executorService.submit(new ConnectionHandler(socket));

            }

        } catch (IOException e) {
            logger.error(e);

        } finally {
            close();
        }

    }

    private void close() {
        try {
            DatabaseService.getInstance().close();

            if (mServerSocket != null) {
                mServerSocket.close();
            }

        } catch (IOException e) {
            logger.error(e);
        }
    }

    private boolean isConnected() {
        return mServerSocket != null && !mServerSocket.isClosed();
    }

}

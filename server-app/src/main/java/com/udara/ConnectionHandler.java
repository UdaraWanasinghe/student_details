package com.udara;

import com.udara.DataModels.StudentData;
import com.udara.DataModels.StudentDataReply;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class ConnectionHandler implements Runnable {

    private Socket socket;
    private InputStream mInputStream;
    private OutputStream mOutputStream;

    private Logger logger = LogManager.getLogger(ConnectionHandler.class);

    ConnectionHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        try {
            mInputStream = new BufferedInputStream(socket.getInputStream());
            mOutputStream = new BufferedOutputStream(socket.getOutputStream());

            while (true) {

                String json = readJson();

                if (json != null) {
                    StudentData studentData = Codec.decodeStudentData(json);

                    DatabaseService databaseService = DatabaseService.getInstance();

                    boolean isSuccess = databaseService.create(studentData);

                    if (isSuccess) {
                        logger.debug("Insertion success");

                        String encodedData = Codec.encodeReplyData(new StudentDataReply("Insertion successful"));
                        sendReply(encodedData);

                    } else {
                        logger.debug("Insertion failed");

                        String encodedData = Codec.encodeReplyData(new StudentDataReply("Insertion failed"));
                        sendReply(encodedData);

                    }
                }else{
                    stop();
                    break;
                }
            }


        } catch (Exception e) {
            logger.error(e);
        }

    }


    /**
     * Read from input stream until pattern found
     *
     * @return pattern found
     * @throws IOException if cannot read from input stream
     */
    private String readJson() throws IOException {
        byte[] buffer = new byte[1024];

        int read;

        StringBuilder stringBuilder = new StringBuilder();

        while ((read = mInputStream.read(buffer)) != -1) {
            if (read == 0) continue;

            stringBuilder.append(new String(buffer, 0, read));

            String str = stringBuilder.toString();

            String matched = PatternMatcher.findJsonPattern(str);

            if (matched != null) {
                logger.debug("Pattern: " + matched);

                return matched;

            } else {
                logger.debug("Matched pattern returned null for string: " + str);
            }
        }

        return null;
    }

    private void stop() throws IOException {
        socket.close();

    }

    private void sendReply(String message) {
        try {
            mOutputStream.write(message.getBytes());
            mOutputStream.flush();

            logger.debug("Reply sent: " + message);

        } catch (IOException e) {
            logger.debug(e.getMessage());
        }
    }
}

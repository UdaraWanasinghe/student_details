package com.udara;

import com.google.gson.Gson;
import com.udara.DataModels.StudentData;
import com.udara.DataModels.StudentDataReply;

class Codec {

    private Codec() {
    }

    static String encodeReplyData(StudentDataReply studentDataReply) {
        Gson gson = new Gson();
        return gson.toJson(studentDataReply);
    }

    static StudentData decodeStudentData(String data) {
        Gson gson = new Gson();
        return gson.fromJson(data, StudentData.class);
    }

    /**
     * convert StudentData object into JSON String
     *
     * @param studentData data to convert
     * @return json string
     */
    static String encodeStudentData(StudentData studentData) {
        Gson gson = new Gson();
        String dataString = gson.toJson(studentData);
        return dataString;
    }

    /**
     * Convert data string to StudentData object
     *
     * @param dataString
     * @return
     */
    static StudentDataReply decodeReplyData(String dataString) {
        Gson gson = new Gson();
        StudentDataReply studentDataReply = gson.fromJson(dataString, StudentDataReply.class);
        return studentDataReply;
    }
}

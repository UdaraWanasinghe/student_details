package com.udara;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class PatternMatcher {
    private static Pattern mPattern = Pattern.compile("\\{(.)+}");

    static String findJsonPattern(String str) {
        Matcher matcher = mPattern.matcher(str);

        if (matcher.find()) {
            return matcher.group();
        }

        return null;
    }
}

package com.udara.DataModels;

public class StudentData {
    public String indexNo;
    public String name;
    public String address;
    public String age;

    public StudentData(String indexNo, String name, String address, String age) {
        this.indexNo = indexNo;
        this.name = name;
        this.address = address;
        this.age = age;
    }

}
